FROM westsoft/php:7.2-nginx

USER westsoftware

RUN sudo apk add --update --no-cache php7-bcmath

WORKDIR /var/www/app
COPY . .

RUN sudo chown -R westsoftware:westsoftware /var/www/app

RUN composer install

EXPOSE 8080
